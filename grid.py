from typing import List, cast
import itertools
from math import sqrt, ceil, dist

from agents import Patch, Settlement, Household


class Grid:
    def __init__(self, width, height):
        self.width = width
        self.height = height

        self.grid = []
        for x in range(self.width):
            col = []
            for y in range(self.height):
                col.append(self.default_val())
            self.grid.append(col)
        
        # set of cells with no one living on
        self.no_settlements = set(itertools.product(*(range(self.width), range(self.height))))
        # for cacheing certain commands
        self._cache = {}

    def default_val(self):
        return {'patch': None, 'settlement': None, 'households': []}

    def __getitem__(self, index):

        if isinstance(index, int):
            # grid[x]
            return self.grid[index]

        x, y = index
        if isinstance(x, int) and isinstance(y, int):
            # grid[x,y]
            return self.grid[x][y]
        if isinstance(x, int):
            # grid[x, :]
            return self.grid[x]
        if isinstance(y, int):
            # grid[:, y]
            return [row[y] for row in self.grid]

        # grid[:, :]
        x, y = (cast(slice, x), cast(slice, y))
        cells = []
        for rows in self.grid[x]:
            for cell in rows[y]:
                cells.append(cell)
        return cells

    def place_agent(self, agent, pos):
        x, y = pos
        if type(agent) == Patch:
            self.grid[x][y]['patch'] = agent
            agent.pos = pos
        elif type(agent) == Settlement:
            self.grid[x][y]['settlement'] = agent
            agent.patches.append(pos)
            self.no_settlements.discard(pos)
        elif type(agent)== Household:
            self.grid[x][y]['households'].append(agent)
            agent.pos = pos
        
    def remove_agent(self, agent, pos):
        # should implement safety guards here so that we can't remove
        # agent that isn't the actual agent at the cell pos

        x, y = pos
        
        if type(agent) == Patch:
            self.grid[x][y]['patch'] = None
        elif type(agent) == Settlement:
            self.grid[x][y]['settlement'] = None
            agent.patches.remove(pos)
            self.no_settlements.add(pos)
        else:
            self.grid[x][y]['households'].remove(agent)
            agent.pos = None

    def has_settlement(self, pos):
        x, y = pos
        return self.grid[x][y]['settlement'] is not None

    def get_settlement(self, pos):
        x, y = pos
        return self.grid[x][y]['settlement']

    def get_patch(self, pos):
        x, y = pos
        return self.grid[x][y]['patch']
    
    def get_patches(self, pos_list):
        return [self.get_patch(pos) for pos in pos_list]

    def get_settlement(self, pos):
        x, y = pos
        return self.grid[x][y]['settlement']
    
    def get_settlements(self, pos_list):
        return [self.get_settlement(pos) for pos in pos_list]

    def iter_no_settlement_patches(self):
        return iter(self.get_patches(self.no_settlements))

    def dist_between_patches(self, patch1, patch2):
        #return math.sqrt( (patch2.center[0] - patch1.center[0])**2 + (patch2.center[1] - patch1.center[1])**2 )
        return patch1.polygon.centroi.distance(patch2.polygon.centroid)  # returns distance in lat/lon degree

    def dist_between_patches_km(self, patch1, patch2):
        return patch1.polygon.centroid.distance(patch2.polygon.centroid) * 100

    def dist_between_pos_patches(self, pos1, pos2, in_km=True):
        patch1 = self.get_patch(pos1)
        patch2 = self.get_patch(pos2)
        if in_km:
            return self.dist_between_patches_km(patch1, patch2)
        else:
            return self.dist_between_patches(patch1, patch2)

    def dist_between_pos_xy(self, pos1, pos2):
        return dist(pos1, pos2)
    
    def out_of_bounds(self, pos):
        x, y = pos
        return x < 0 or x >= self.width or y < 0 or y >= self.height

    def get_neighbor_cells(self, pos, radius, include_self=True, hole_radius=0):
        # try cache first:
        cache_key = ('get_neighbor_cells', pos, radius, include_self, hole_radius)
        neighborhood = self._cache.get(cache_key, None)

        # command hasn't been run
        if neighborhood is None:
            coords = []
            x, y = pos
            if hole_radius > 0:
                xyrange = set(range(-hole_radius - radius, hole_radius + radius+1))
                hole = set(range(-radius, radius+1))
                xyrange = xyrange - hole
            else:
                xyrange = range(-radius, radius+1)
            for dx in xyrange:
                for dy in xyrange:
                    coord = (x + dx, y + dy)
                    if not (dx==0 and dy==0 and not include_self) and not self.out_of_bounds(coord):
                        coords.append(coord)

            neighborhood = sorted(coords)
            self._cache[cache_key] = neighborhood
                        
        return neighborhood

    def iter_neighbor_cells(self, pos, radius, include_self=True, hole_radius=0):
        cell_list = self.get_neighbor_cells(pos, radius, include_self, hole_radius)
        return filter(None, (self.grid[x][y] for x, y in cell_list))

    def iter_cell_agents(self, agent_type, cell_list):
        return filter(None, (self.grid[x][y][agent_type] for x, y in cell_list))

    def iter_neighbor_agents(self, agent_type, pos, radius, include_self=True):
        nbhd = self.get_neighbor_cells(pos, radius, include_self)
        return self.iter_cell_agents(agent_type, nbhd)

    def iter_all_agents(self, agent_type):
        cell_list = itertools.product(*(range(self.width), range(self.height)))
        #print(list(cell_list))
        return self.iter_cell_agents(agent_type, cell_list)

    def iter_all_cells(self):
        cell_list = itertools.product(*(range(self.width), range(self.height)))
        return filter(None, (self.grid[x][y] for x, y in cell_list))

    def get_n_closest_patches(self, pos, n):
        # integer radius of moore neighborhood to get at least n cells (including self)
        r = ceil((sqrt(n+1) - 1) / 2)
        # get the neighbor cells
        neighbor_cells = self.get_neighbor_cells(pos, r, include_self=False)
        # sort by distance and get only first n
        neighbor_cells = sorted(neighbor_cells, key=lambda c: dist(pos, c))[:n]
        # return an iterator of those n patches
        return self.iter_cell_agents('patch',neighbor_cells)

    def get_n_closest_patches_where(self, pos, n, func, init_search_radius):
        """Returns n closest Patches p from pos where func(p) evaluates to True;
        func needs to be a function of a grid cell"""

        candidate_area = set()
        search_radius = init_search_radius
        hole_radius = 0
        while len(candidate_area) < n:
            candidate_cells = self.iter_neighbor_cells(pos, search_radius, False, hole_radius)
            candidate_patches = set([c['patch'] for c in candidate_cells if func(c)])
            candidate_patches = candidate_patches - candidate_area # remove cells already in candidate_area
            needed = int(n - len(candidate_area))
            if len(candidate_patches) >= needed:
                candidate_patches = sorted(candidate_patches, key=lambda p: dist(pos, p.pos))[:needed]
                candidate_area.update(candidate_patches)
            else:
                candidate_area.update(candidate_patches)
                hole_radius = search_radius
                search_radius = search_radius + 5
        return candidate_area

    def num_individuals_in_cell(self, pos):
        x,y = pos
        return sum(hh.ind for hh in self.grid[x][y]['households'])

    def draw(self):
        pass