from collections import defaultdict
from re import search
from typing import Set
import numpy as np
from numpy import random, uint64
import pandas as pd
import networkx as nx
import itertools
from math import ceil, sqrt, dist, exp
import pickle

from helper import prepare_environment
from grid import Grid
from agents import Patch, Settlement, Household


class Model:

    def __init__(self, seed,
                raster_path,            # path to elevation raster based on which grid will be created
                rivers_shp_path,        # path to shapefile containing river lines
                springs_shp_path,       # path to shapefile containing spring locations
                sites_shp_path,         # path to shapefile containing sites locations
                resource_distribution,  # 'uniform' or 'normal'
                max_ind_cell=250,        # max number of individuals / cell
                max_ind_household=10,   # max individuals / household
                min_resource=250,       # minimum resource needed for an individual
                yrs_storage=5,          # years of storage
                migration_radius=40,    # radius of migration in km (will be converted to lat/long degree)
                aquifer_radius=2.25,    # radius in aquifer productivity decay
                rdL=0.6,                # rel. diff. ratio for re-organization relationship evaluation
                rbirth=0.003,           # birth rate
                rdeath=0.002,           # death rate
                pct_surplus_pooled=0.2, # fraction of surplus a household puts into settlement trade pool
                lmbda=0.2,              # gravity model parameter
                enable_draw=False,      # enable drawing of environment (will require more memory)
                ):
        
        # set seed for random number generator
        self.seed = seed
        self.rng = random.default_rng(seed)

        # initialize time step
        self.tick = 0

        # set cell size in km
        # each cell in grid will be 0.001 degree apart (~111m)
        # so each cell is ~0.111 km
        self.cell_size = 0.100 #km
        # note: I think the authors took each cell to be 0.1 km exactly

        # set other model parameters
        self.aquifer_radius = aquifer_radius
        self.max_hh_cell = int(max_ind_cell / max_ind_household)    # max households per patch
        self.max_ind_cell = max_ind_cell
        self.max_ind_household = max_ind_household
        self.min_resource = min_resource
        self.yrs_storage = yrs_storage
        self.migration_radius = int(migration_radius / self.cell_size)   # migration radius in terms of cells
        # TODO: if migration radius is big enough, we should set this to be the entirety of the grid for efficiency
        self.rdL = rdL
        self.rbirth = rbirth
        self.rdeath = rdeath
        self.pct_surplus_pooled = pct_surplus_pooled
        self.lmbda = lmbda

        # set up variables to store timeseries statistics
        self.stat_households = []
        self.stat_settlements = []
        self.stat_active_settlements = []
        self.stat_pop = []
        self.stat_avg_hh_per_settlement = []
        self.stat_avg_storage = []
        self.stat_avg_utility = []
        self.stat_hunger = []       # % households with utility < uThres
        self.stat_movements = []    # migrations per year
        self.stat_growth_rate = []
        self.stat_in_degree_index = []
        self.stat_out_degree_index = []
        self.stat_betweenness_index = []

        # set up grid, patches, and historical sites data
        self.grid = None
        self._sites = None
        # note: the following function places a Grid object to self.grid and a DataFrame to self._sites
        self._setup_environment(raster_path, rivers_shp_path, springs_shp_path, sites_shp_path, resource_distribution, enable_draw)


        print("\nSetting up sites, settlements and households...")
        # generate initial settlements and households and place into grid
        self.settlements = {}   # for storing all Settlement objects in this model run
        self.households = []    # for stroing all Household objects in this model run
        
        # --- get current sites to establish possible locations; set site importance, etc
        print("Establishing archaeological sites known at t=0...")
        self._establish_current_sites()
        self._place_current_sites()
        # --- generate initial settlements on the patches that have has_site==True
        # --- (settlement id should be grouped by site id)
        # --- then, generate households and place into cell and settlement
        # patches = [p for p in self.grid.iter_all_agents('patch') if p.has_site]
        # print(f"# of patches with sites at t=0: {len(patches)}")
        # for patch in patches:

        #     # first, check if a settlement has already been created for the site on this patch
        #     settlement_agent = self._check_create_settlement_on_patch(patch)

        #     pos = patch.pos
        #     site_id = patch.site_id

        #     # next, generate (random) number of initial households in this patch:
        #     nh = self.rng.integers(1, self.max_hh_cell+1)
        #     # number of individuals for each household
        #     nind = self.rng.integers(1, max_ind_household+1, size=nh)

        #     # place household agents into cell and add households to the settlement
        #     household_agents = [Household(site_id, nind[i], min_resource, yrs_storage) for i in range(nh)]
        #     for agent in household_agents:
        #         self.grid.place_agent(agent, pos)
        #         settlement_agent.add_household(agent)
        #         self.households.append(agent)

        # --- update settlement agents
        for settlement_agent in self.settlements.values():
            settlement_agent.update()
        
        num_init_settlements = len(self.settlements.keys())
        num_init_households = sum(s.num_households for s in self.settlements.values())
        num_init_ind = sum(s.num_ind for s in self.settlements.values())
        print(f"# of initial settlements: {num_init_settlements}")
        print(f"# of initial households: {num_init_households}")
        print(f"# of initial individuals: {num_init_ind}")

        print(f"\npeek cell at (122,174) on grid: {self.grid[122][174]}")

        print("\nSetup finished.")



    """call Model.run(t) to run the model for t years"""
    def run(self, years=2000):
        

        # ============== MIGRATION MODULE ============== #
        def migration():

            migrating_households = [hh for hh in self.households if hh.considers_migrating]
            # end migration process (return) if no household is migrating
            mgh = len(migrating_households)
            if mgh == 0:
                print(f"{mgh} households considered moving this year; {0} migrated")
                self.stat_movements.append(0)
                return 
            
            # find candidate migration patches
            def ok_to_migrate(cell):
                return ((cell['patch'].has_site or cell['settlement'] is not None)\
                    and (not cell['patch'].is_sea) and (cell['patch'].aquifer_utility > 0) 
                    and cell['settlement'].num_ind <= 240)

            for cell in self.grid.iter_all_cells():
                cell['patch'].ok_to_migrate = True if ok_to_migrate(cell) else False

            # the patches should be at least 2 cells away from any existing settlement
            # otherwise agents may also move to the respective settlement
            # (the latter is done by setting include_self=False in going through neighbors of settlements)
            # for cell in self.grid.iter_all_cells():
            #     if cell['patch'].ok_to_migrate and cell['settlement'] is not None:
            #         for patch in self.grid.iter_neighbor_agents('patch', cell['patch'].pos, 2, include_self=False):
            #             patch.ok_to_migrate = False

            candidate_patches = [patch for patch in self.grid.iter_all_agents('patch') if patch.ok_to_migrate]
            
            migration_count = 0
            # for each household with migrating == True:
            for household in migrating_households:
                household.considers_migrating = False
                # calculate its current utility per cultivating cell
                cau = household.utility_last_year / max([household.num_cpatches,1])
                hhpos = household.pos
                #print(f"Household {household.uid} with utility {cau:.1f}kg/cell is considering migrating from settlement {household.sid} at {hhpos}")

                # for each patch in migration candidate patches,
                # calculated production given distance from household (in km)
                # production_temp = production * exp( -(dist(patch,household) / 10) * 0.005 )
                for patch in candidate_patches:
                    ppos = patch.pos
                    #relocation_cost = exp(-(self.grid.dist_between_pos_patches(hhpos,ppos)/10)*0.005)
                    relocation_cost = 1
                    patch.production = patch.agr_production(household.ind, self.max_ind_cell)
                    patch.production = relocation_cost * patch.production
                
                # calculate expected production which is avg over neighbors times aquifer_utility
                for patch in candidate_patches:
                    pos = patch.pos
                    production_of_neighbors = [p.production for p in self.grid.iter_neighbor_agents('patch',pos,1)]
                    patch.expected_utility = np.mean(production_of_neighbors)
                    #patch.expected_utility = patch.aquifer_utility * patch.expected_utility

                # for migration candidate patches with settlements,
                # calculate avg utility per cell for a household agent within
                # expected utility for a settlement location is the avg of avg utility
                # set expected production of patch to be the expected utility for settlement
                for patch in candidate_patches:
                    pos = patch.pos
                    if (not self.grid.has_settlement(pos)):
                        continue
                    settlement = self.grid.get_settlement(pos)
                    if len(settlement.households) > 0:
                        est_util = sum(hh.utility_last_year / hh.sensory_range for hh in settlement.households)
                        est_util = est_util / settlement.num_households
                        patch.expected_utility = est_util

                # mpatch = one of migration candidate patches with highest expected production within migration radius
                mpatch = max(candidate_patches, key=lambda p: max([p.expected_utility,p.production_temp]))
                #print(f"--> candidate patch {mpatch.pos} has expected utility {mpatch.expected_utility:.1f}")

                # if expected utility of mpatch > current utility, migrate
                if mpatch.expected_utility > cau:
                    migration_count += 1
                    new_pos = mpatch.pos
                    # if settlement already exists here, use this settlement id
                    # in case nearby patches have to be chosen due to restrictions on num individuals
                    # since the new chosen will be an extension of the previous settlement
                    if self.grid.has_settlement(new_pos):
                        use_this_settlement_id = self.grid.get_settlement(new_pos).uid
                    elif mpatch.has_site:
                        use_this_settlement_id = mpatch.site_id
                    else:
                        use_this_settlement_id = None

                    # check for number of households/individuals restrictions of mpatch
                    # if exceed, move to the closest patch with num individuals less than threshold
                    mc = self.max_ind_cell
                    mh = self.max_ind_household
                    def available(cell):
                        x,y = cell['patch'].pos
                        return sum(hh.ind for hh in cell['households']) < (mc-mh)
                    while self.grid.num_individuals_in_cell(new_pos) >= (mc-mh):
                        mpatch = self.grid.get_n_closest_patches_where(mpatch.pos, 1, available, 1).pop()
                        new_pos = mpatch.pos

                    # remove household from current patch and settlement
                    self.grid.remove_agent(household, hhpos)
                    old_settlement = self.settlements[household.sid]
                    if household.sid != use_this_settlement_id:
                        old_settlement.remove_household(household)
                        if len(old_settlement.households) == 0:
                            old_settlement.is_active = False
                    # check for settlement at new patch (create if not already exist)
                    # and add household to settlement
                    new_settlement = self._check_create_settlement_on_patch(mpatch, use_this_settlement_id)
                    new_settlement.add_household(household)
                    # place household in new patch
                    self.grid.place_agent(household, new_pos)
                    # set household's years to migration to 0
                    household.years_to_migrate = 0

                    #print(f"--> Household {household.uid} migrated to {new_pos} (new settlement: {new_settlement.uid})")

                # TODO: destroy/create relationships
            print(f"{mgh} households considered moving this year; {migration_count} migrated")
            self.stat_movements.append(migration_count)



        # ============== CULTIVATION MODULE ============== #
        def cultivate_harvest():

            def _can_be_cultivated(grid_cell):
                """a helper function to evaluate cells that can be cultivated"""
                return grid_cell['settlement'] is None and not grid_cell['patch'].is_cultivated and not grid_cell['patch'].is_sea
            
            # for each settlement:
            for settlement in self.settlements.values():
                #print(f"Households in settlement {settlement.uid} are cultivating...")

                # 1) calculate total number of cells households need to cultivate
                pos = settlement.patches[0] # first patch in settlement
                ncp = settlement.num_cultivating_patches

                # 2) choose candidate area to cultivate (uncultivated patches nearby where there's no settlement)
                r1 = ceil(settlement.approx_radius)
                r2 = ceil(((sqrt(ncp)-1)/2) - settlement.approx_radius)
                init_search_radius = r1 + r2 + 5
                candidate_area = self.grid.get_n_closest_patches_where(
                    pos, ncp, _can_be_cultivated, init_search_radius)
                #print(f"candidate area of {len(candidate_area)} patches: {[p.pos for p in candidate_area]}")

                # 3) each household in the settlement takes turn harvesting
                # TODO: implements social ranking so superior agents get to go first
                for household in settlement.households:

                    # 3.1) household choose sensory_range uncultivated patches from candidate area where resource is highest; 
                    # these patch becomes unavailable to the next household agents
                    available_patches = [p for p in candidate_area if not p.is_cultivated]
                    sensory_range = int(household.sensory_range)
                    if len(available_patches) > sensory_range:
                        cpatches = sorted(available_patches, key=lambda p: p.resource, reverse=True)[:sensory_range]
                    else:
                        cpatches = available_patches

                    # hosuehold cultivates from each patch and adds harvest to utility
                    # TODO: if harvest less than u_thres, do something about storage and migration here
                    utility = 0
                    for patch in cpatches:
                        # agricultural production depends on number of individuals in the cell
                        #cell_pop = self.grid.num_individuals_in_cell(patch.pos)
                        cell_pop = self.grid.num_individuals_in_cell(household.pos)
                        harvest = patch.cultivate(cell_pop, self.max_ind_cell)
                        utility += harvest

                    # update household after it finishes harvesting everything
                    #print(f"Household {household.uid} harvested {utility:.1f} kg of product from {[p.pos for p in cpatches]}")
                    household.utility += utility
                    household.num_cpatches = len(cpatches)
                    



        # ============== INTRA-SETTLEMENT (EGALITARIAN) TRADE MODULE ============== #
        def share_resources():
            for sid, settlement in self.settlements.items():
                if settlement.is_active:
                    pool = 0
                    for household in settlement.households:
                        pool += household.utility
                        household.utility = 0
                    share_amount = pool / settlement.num_ind
                    #print(f"{sid} is sharing {pool:.1f} kg of harvest among {settlement.num_ind} individuals ({share_amount:.1f} kg each)")
                    for household in settlement.households:
                        household.utility = share_amount * household.ind




        # ============== INTER-SETTLEMENT TRADE MODULE ============== #
        def intersettlement_trade():

            G = nx.DiGraph()
            G.add_nodes_from(list(self.settlements.keys()))
            nS = 0  # number of settlements with reciprocated edges
            
            # calculate interaction probabilities (gravity model) for all pairs of settlements
            # I_ij = importance_j / dist(i,j)**lambda
            probs = defaultdict(dict)
            for sid1, s1 in self.settlements.items():
                for sid2, s2 in self.settlements.items():
                    if (sid1 != sid2):
                        pos1 = s1.patches[0] # first established patch of this settlement
                        pos2 = s2.patches[0]
                        dist = self.grid.dist_between_pos_patches(pos1,pos2,in_km=True)
                        probs[sid1][sid2] = s2.ws / (dist**self.lmbda)
                    else:
                        probs[sid1][sid2] = 0

            pct_pooled = self.pct_surplus_pooled
            # for each settlement i:
            for sid1, s1 in self.settlements.items():
                if not s1.is_active:
                    continue
                # households with surplus pool resources and subtract from their storage
                pool = 0
                for household in s1.households:
                    surplus = sum(household.storage)
                    if surplus > 0:
                        # add pct_pooled % of surplus to settlement pool
                        contribution = pct_pooled * surplus
                        pool += contribution
                        # subtract from storage by removing the oldest resources first
                        i = self.yrs_storage - 1
                        while (contribution > 0) and (i >= 0):
                            amount = household.storage[i]
                            if contribution - amount >= 0:
                                household.storage[i] = 0
                                contribution = contribution - amount
                            else:
                                household.storage[i] = amount - contribution
                                contribution = 0
                            i = i-1
                
                # trade with other settlements according to interaction probabilities
                distr_ws = sum(probs[sid1].values())
                for sid2, s2 in self.settlements.items():
                    # determine whether to trade with s2
                    I_ij = probs[sid1][sid2]
                    #print(f"trading probability between {sid1} and {sid2}: {I_ij}")
                    if self.rng.random() < I_ij and s2.is_active:
                        # if trade, determine amount of resource and perform trade
                        trade_amount = (I_ij * pool) / distr_ws
                        #print(f"{sid1} trades {trade_amount} kg of resources with {sid2}")
                        s2.exchange_received += trade_amount
                        # add directed edges to network
                        G.add_edge(sid1, sid2, weight=trade_amount)
                        # if both s1 and s2 reciprocate in the exchange, add to nS
                        # (will be used to calculate betweenness centrality index later)
                        if G.has_edge(sid2, sid1):
                            nS += 2  


            # after trade, each household distributes public good payoff to households
            for settlement in self.settlements.values():
                if settlement.is_active:
                    damount = settlement.exchange_received / settlement.num_ind
                    for household in settlement.households:
                        payoff = damount * household.ind
                        household.storage[0] += payoff
                    settlement.exchange_received = 0


            # compute network statistics
            # --- relative in-degree centrality index
            n = G.order()
            in_dgr_ctr = list(nx.in_degree_centrality(G).values())
            max_in_dgr = max(in_dgr_ctr)
            in_degree_index = sum([max_in_dgr - c for c in in_dgr_ctr]) / (n-2)

            # --- relative out-degree centrality index
            out_dgr_ctr = list(nx.out_degree_centrality(G).values())
            max_out_dgr = max(out_dgr_ctr)
            out_degree_index = sum([max_out_dgr - c for c in out_dgr_ctr]) / (n-2)

            # --- relative between centrality index
            nI = sum(1 for x in in_dgr_ctr if x>0)
            nO = sum(1 for x in out_dgr_ctr if x>0)
            rel_norm = (nI-1)*(nO-1) - (nS-1)
            btw_ctr = list(nx.betweenness_centrality(G, normalized=False).values())
            btw_ctr = [x/rel_norm for x in btw_ctr]
            max_btw_ctr = max(btw_ctr)
            betweenness_index = sum([max_btw_ctr - c for c in btw_ctr]) / (n-1)

            self.stat_in_degree_index.append(in_degree_index)
            self.stat_out_degree_index.append(out_degree_index)
            self.stat_betweenness_index.append(betweenness_index)
            
            



        # ============== POPULATION MODULE ============== #
        def population_dynamics():

            deaths = 0
            births = 0
            pop_size = sum(s.num_ind for s in self.settlements.values())

            for household in self.households:
                if self.rng.random() < self.rdeath:
                    deaths += 1
                    if household.ind <= 1:
                        # household dies
                        self.households.remove(household)
                    else:
                        household.remove_individual()
                
                u_hat = min([household.utility_last_year, household.u_thres])
                birth_rate = self.rbirth * u_hat/household.u_thres
                if self.rng.random() < birth_rate:
                    births +=1
                    household.add_individual()
                    # if number of individuals exceed max_ind_household
                    if household.ind > 10:
                        # split to generate an offspring household
                        offspring = household.split()
                        # add household to model
                        self.households.append(offspring)
                        # if current cell is full, place offspring in nearby available cell
                        pos = household.pos
                        patch = self.grid.get_patch(pos)
                        m = self.max_ind_cell
                        new_inds = offspring.ind
                        existing_inds = self.grid.num_individuals_in_cell(pos)
                        def available(cell):
                            # helper function to help place offspring households in available cells
                            x,y = cell['patch'].pos
                            return  (new_inds + existing_inds) < m
                        while (self.grid.num_individuals_in_cell(pos) + offspring.ind) >= self.max_ind_cell:
                            patch = self.grid.get_n_closest_patches_where(pos, 1, available, 1).pop()
                            pos = patch.pos
                        self.grid.place_agent(offspring, pos)
                        # add offspring agent to settlement
                        settlement = self.settlements[household.sid]
                        settlement.add_household(offspring)
                        # check if new patch and whether it already has settlement established
                        if offspring.pos != household.pos:
                            self._check_create_settlement_on_patch(patch, offspring.sid)

                        print(f"Household {household.uid} at {household.pos} generated an offspring {offspring.uid} at {offspring.pos}")

            net_growth = births - deaths
            growth_rate = round((net_growth / pop_size) * 100, 3)
            print(f"births - deaths: {net_growth}; growth rate: {growth_rate}%")
            self.stat_growth_rate.append(growth_rate)
        # ====================== END MODULES ====================== #



        while self.tick < years:
            print(f"--- Year {3100 - self.tick} BC")
            #print(f"349: {[hh.uid for hh in self.settlements[349].households]}")
            #print(f"515: {[hh.uid for hh in self.settlements[515].households]}")
            #print(f"593: {[hh.uid for hh in self.settlements[593].households]}")
            if self.tick > 0:
                #print("establishing current sites...")
                self._establish_current_sites()
                self._place_current_sites()
                #print("begin migration process...")
                migration()
            for settlement in self.settlements.values():
                settlement.check_active()
            #print("begin cultivation...")
            cultivate_harvest()
            #print("begin sharing resources...")
            share_resources()
            #print("begin intersettlement trade...")
            intersettlement_trade()
            for household in self.households:
                household.update()
            for settlement in self.settlements.values():
                settlement.update()
            for patch in self.grid.iter_all_agents('patch'):
                patch.update()
            #print("begin population dynamics...")
            population_dynamics()

            # collect statistics
            count_hh = len(self.households)
            count_settlements = len(self.settlements.values())
            count_active_settlements = len([s for s in self.settlements.values() if s.is_active])
            self.stat_households.append(count_hh)
            self.stat_settlements.append(count_settlements)
            self.stat_active_settlements.append(count_active_settlements)
            self.stat_pop.append(sum([s.num_ind for s in self.settlements.values()]))
            self.stat_avg_hh_per_settlement.append((count_hh/count_settlements))
            avg_storage = np.mean([sum(hh.storage) for hh in self.households])
            self.stat_avg_storage.append(avg_storage)
            u = [hh.utility_last_year for hh in self.households]
            uthres = [hh.u_thres for hh in self.households]
            avg_utility = np.mean(u)
            hunger = sum(1 if u < uthres else 0 for u,uthres in zip(u,uthres)) / count_hh
            self.stat_avg_utility.append(avg_utility)
            self.stat_hunger.append(hunger)       # % households with utility < uThres

            self.tick += 1
            print("\n")





# ===================================================================== #
#
#                   AREA FOR PRIVATE HELPER FUNCTIONS
# 
# _setup_environment : 
#   helps the constructor calculate environmental attributes of Patches
#
# _establish_current_sites :
#   gets sites known at the model's time tick and 
#   establish these sites on the appropriate locations (Patches)
#
# _check_create_settlement_on_patch :
#   checks if a settlement has already been created on this patch;
#   creates one if not
#
# ===================================================================== #


    def _setup_environment(self, 
                            raster_path, 
                            rivers_shp_path, 
                            springs_shp_path, 
                            sites_shp_path,
                            resource_distribution,
                            enable_draw):
        """
        Sets up the model environment given the raster and shapefiles.
        This function
            - creates a grid
            - creates and place patches into a grid
            - calculates and set patch attributes based on geo-morphological information
            - processes archaeological sites information
            - sets values to self.grid and self._sites so data can be read by the controller instance
        """


        # process geo-morphological information (see helper.py)
        grid_dim, patches, sites = prepare_environment(raster_path, rivers_shp_path, springs_shp_path, sites_shp_path)

        print("\nSetting up model environment...")

        # initialize our grid
        width, height = grid_dim
        grid = Grid(width, height)

        # sample land productivity 
        if resource_distribution == 'uniform':
            productivity = self.rng.uniform(0, 1, len(patches))
        else:
            productivity = self.rng.normal(0.5, 0.2, len(patches))
        pmin = np.min(productivity)
        pmax = np.max(productivity)
        scale = pmax - pmin 
        productivity = (productivity - pmin) / scale

        # create and put patch agents in each cell in the grid
        
        for i, patch in enumerate(patches):
            patch = tuple(patch)
            pos = patch[:2]
            # create a Patch agent
            patch_agent = Patch(*patch[2:])
            # set land productivity on each patch
            patch_agent.land_productivity = productivity[i]
            #if i==0: print(patch_agent)
            # place agent on grid
            grid.place_agent(patch_agent, pos)



        # next: calculate, normalize, and set aquifier utility for all patches
        print("\nCalculating aquifer utility of environmental patches...")

        # try cache first:
        try:
            with open('model_cache/aquifer.pickle', 'rb') as f:
                aquifer = pickle.load(f)
            
            coastline = aquifer['coastline']
            utility = aquifer['utility']
            
            i=0
            has_river_count = 0
            has_spring_count = 0
            for patch in grid.iter_all_agents('patch'):
                patch.is_coastline = coastline[i]
                #patch.rvalue = 1 if p.is_coastline else patch.rvalue
                patch.aquifer_utility = utility[i]
                patch.update_resource()
                if patch.has_river:
                    has_river_count += 1
                if patch.has_spring:
                    has_spring_count += 1
                i+=1
            print(f"# of patches with sea access (coastline): {sum(coastline)}")
            print(f"# of patches with river access: {has_river_count}")
            print(f"# of patches with spring access: {has_spring_count}")


        # if no existing cache, calculate is_coastline and aquifer_utility
        except:

            # for each patch, calculate and set is_coastline attribute using Grid.neighbors
            # is_coastline = True if a patch has at least a neighbor with is_sea = True
            coastline = []
            has_river_count = 0
            has_spring_count = 0
            for patch in grid.iter_all_agents('patch'):
                pos = patch.pos
                is_coastline = sum(1 for n in grid.iter_neighbor_agents('patch', pos, 1) if n.is_sea) > 0
                is_coastline = (is_coastline and not patch.is_sea)
                patch.is_coastline = is_coastline
                patch.rvalue = 1 if is_coastline else patch.rvalue
                coastline.append(is_coastline)
                if patch.has_river:
                    has_river_count += 1
                if patch.has_spring:
                    has_spring_count += 1
            print(f"# of patches with sea access (coastline): {sum(coastline)}")
            print(f"# of patches with river access: {has_river_count}")
            print(f"# of patches with spring access: {has_spring_count}")


            ar = int(self.aquifer_radius / self.cell_size)

            # --- 1. go through patches with access to river or sea
            # --- nearby cells will have decaying utility
            rc = [p for p in grid.iter_all_agents('patch') if (p.has_river or p.is_coastline)]
            sum_density_rc = len(rc)
            rvalues = []
            for p1 in rc:
                for p2 in grid.iter_neighbor_agents('patch', p1.pos, ar):
                    density = (1 / ar**2) * ((3/np.pi) * (1 - (grid.dist_between_patches_km(p1,p2) / ar)**2) ** 2)
                    rvalue = p2.rvalue + (density * sum_density_rc)
                    p2.rvalue = rvalue
                    rvalues.append(rvalue)
            
            # --- 2. go through patches with access to springs
            s = [p for p in grid.iter_all_agents('patch') if p.has_spring]
            sum_density_s = len(s)
            svalues = []
            for p1 in s:
                for p2 in grid.iter_neighbor_agents('patch', p1.pos, ar):
                    density = (1 / ar**2) * ((3/np.pi) * (1 - (grid.dist_between_patches_km(p1,p2) / ar)**2) ** 2)
                    svalue = p2.svalue + (density * sum_density_s)
                    p2.svalue = svalue
                    svalues.append(svalue)

            # --- 3. normalize rvalue and svalue on each patch
            # --- aquifer value is the max of the two normalized quantities
            # --- also, update patch so that patch's initial resource can be calculated
            maxr = max(rvalues)
            maxs = max(svalues)

            utility = []
            for p in grid.iter_all_agents('patch'):
                au = max([p.rvalue/maxr, p.svalue/maxs])
                utility.append(au)
            maq = max(utility)
            utility = [u/maq for u in utility]
            i=0
            for p in grid.iter_all_agents('patch'):
                p.aquifer_utility = utility[i]
                p.update_resource()
                i+=1
            
            aquifer = {}
            aquifer['coastline'] = coastline
            aquifer['utility'] = utility
            with open('model_cache/aquifer.pickle', 'wb') as f:
                pickle.dump(aquifer, f)


        # finally, store grid and sites data in this instance
        self.grid = grid
        self._sites = sites



    def _establish_current_sites(self):
        """Gets sites that exist during the model's current time period (tick);
        Establishes sites on the patches that those sites existed.

        Note: if site is Settlement/Village/Site or City, then the site will be established
        in multiple cells within the catchment area.
        """

        current_sites = self._sites.query(f"time_start <= {self.tick} and time_end >= {self.tick}")
        current_sites = current_sites.to_records()
        for site in current_sites:
            site = tuple(site)
            #print(site)
            # position of site
            x,y = site[1].split()
            x = int(x)
            y = int(y)
            
            # if site hasn't already been established
            if not self.grid[x][y]['patch'].has_site:
                site_id = site[0]
                category = site[2]
                importance = site[-1]
                self.grid[x][y]['patch'].establish_site(site_id, category, importance)
                # set catchment site if a Settlement/Village/Site or City
                if category=='Settlement/Village/Site' or category=='City':
                    # catchment area is 10-25 cells for city and 1-3 for settlement/village/site
                    catchment_size = self.rng.integers(10,26) if category=='City' else self.rng.integers(1,4)
                    #print(f"Setting up site {site_id} ({category}) at patch ({x},{y}) with catchment area of {catchment_size} cells")
                    # get nearest patches and establish settlement
                    for patch in self.grid.get_n_closest_patches((x,y), catchment_size):
                        if patch.is_sea: 
                            continue
                        patch.establish_site(site_id, category, importance)
                        #print(f"including patch {patch.pos} in the site's catchment area...")



    def _check_create_settlement_on_patch(self, patch_agent, existing_settlement_id=None):
        """Checks if a settlement exists on this patch. 
       
        If a settlement already exists, return that settlement;
        if not:
            - create a new Settlement object by site_id
            - place on grid
            - store in self.settlements
            - return the settlement

        Note: a settlement can only exist on a patch with has_site = True;
        will return None if patch_agent.has_site == False
        """

        pos = patch_agent.pos
        # first, check if there is already a settlement agent in this cell
        if self.grid.has_settlement(pos):
            #if pos == (144,120): print(f"{pos} has settlement")
            return self.grid.get_settlement(pos)

        # if there is no historical site at this point, establish a new settlement
        # where uid will be generated from a random id generator
        if not patch_agent.has_site:
            if existing_settlement_id is not None:
                settlement_agent = self.settlements[existing_settlement_id]
            else:
                settlement_agent = Settlement(uid=None, importance=None) # settlement not from historical site
                #settlement_agent.add_catchment(pos)
                self.settlements[settlement_agent.uid] = settlement_agent
            self.grid.place_agent(settlement_agent, pos)
            return settlement_agent

        # if not, check if settlement has already been created for this site
        else:
            if existing_settlement_id is not None:
                settlement_id = existing_settlement_id 
            else:
                settlement_id = patch_agent.site_id
            settlement_agent = self.settlements.get(settlement_id, None)
            if settlement_agent is None:
                # if not, create settlement agents based on site id
                # i.e., settlement id = site id
                #print("new settlement created")
                settlement_agent = Settlement(settlement_id, patch_agent.site_importance)
            # add patch to the catchment area of the settlement
            #settlement_agent.add_catchment(pos)
            self.settlements[settlement_id] = settlement_agent
            # note: we do this to create only 1 settlement agent per 1 "site" 
            # (i.e., multiple cells in 1 site will have the same Settlement object)
            self.grid.place_agent(settlement_agent, pos)
        
            return settlement_agent


    def _place_current_sites(self):
        # --- generate initial settlements on the patches that have has_site==True
        # --- (settlement id should be grouped by site id)
        # --- then, generate households and place into cell and settlement
        patches = [p for p in self.grid.iter_all_agents('patch') if p.has_site]
        
        if self.tick == 0:
            print(f"# of patches with sites at t=0: {len(patches)}")
        
        for patch in patches:

            # first, check if a settlement has already been created for the site on this patch
            settlement_agent = self._check_create_settlement_on_patch(patch)

            if (self.tick == 0):
                pos = patch.pos
                site_id = patch.site_id

                # next, generate (random) number of initial households in this patch:
                nh = self.rng.integers(1, self.max_hh_cell)
                # number of individuals for each household
                nind = self.rng.integers(1, self.max_ind_household+1, size=nh)

                # place household agents into cell and add households to the settlement
                household_agents = [Household(site_id, nind[i], self.min_resource, self.yrs_storage) for i in range(nh)]
                for agent in household_agents:
                    self.grid.place_agent(agent, pos)
                    settlement_agent.add_household(agent)
                    self.households.append(agent)