import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Polygon
import rasterio as rio
import richdem as rd


def read_shp(shp_path, proj=None):
    gdf = gpd.read_file(shp_path)
    # set CRS to WGS84
    gdf = gdf.set_crs(4326, allow_override=True)
    if proj:
        gdf = gdf.to_crs(proj)
    return gdf


def make_cells_from_raster(raster_path):

    ras = rio.open(raster_path)
    rd_ras = rd.LoadGDAL(raster_path)
    left, bottom, right, top = ras.bounds
    cell_size = 0.001
    cell_size_m = 100 # in meters
    
    # get numpy array of raster
    band = ras.read(1) 
    # flip raster to read from bottom left (last row comes first)
    band = np.flip(band, 0)

    # get grid dimensions
    rows, cols = band.shape
    grid_dim = (cols, rows) # (width, height)

    # calculate slope
    slope = rd.TerrainAttribute(rd_ras, attrib='slope_riserun') / (8 * cell_size_m) # 8 is picked arbitrarily
    slope = np.flip(slope, 0)

    # construct a GeoDataFrame containing patches as polygons and patches' attributes
    cells = {'patch_id':[], 'x':[], 'y':[], 'elevation': [], 'slope':[], 'geometry':[]}
    xrange = np.arange(left, right, cell_size)
    yrange = np.arange(bottom, top, cell_size)
    for j,y in enumerate(yrange):
        for i,x in enumerate(xrange):
            # make grid Polygon from corner points of cell (bottom left -> bottom right -> top right -> top left)
            pts_coords = [(x,y), (x+cell_size,y), (x+cell_size,y+cell_size), (x,y+cell_size)]
            cell = Polygon(pts_coords)

            elev = band[j][i]
            cell_slope = slope[j][i]

            cells['geometry'].append(cell)
            cells['elevation'].append(elev)
            cells['slope'].append(cell_slope)
            
            # for forming a grid in the ABM later
            cells['x'].append(i)
            cells['y'].append(j)
            cells['patch_id'].append(f'{i} {j}')

    cells_df = gpd.GeoDataFrame(cells, crs=4326)
    return cells_df, grid_dim


def make_patches_for_grid(cells_gdf, rivers_gdf, springs_gdf):
    # perform spatial joins to intersect rivers and springs with cells
    joined = gpd.sjoin(cells_gdf, rivers_gdf[['RIVERS_ID','geometry']], how='left')
    joined = joined.drop(columns=['index_right'])
    joined = gpd.sjoin(joined, springs_gdf[['LAYER','geometry']], how='left')
    joined = joined.drop(columns=['index_right'])

    # create boolean columns for has_river, has_spring, and is_sea based on the intersection
    joined['has_river'] = joined['RIVERS_ID'].apply(lambda x: True if x == x else False) 
    joined['has_spring'] = joined['LAYER'].apply(lambda x: True if x == x else False)
    joined['is_sea'] = ~(joined['elevation'] > 0)
    joined = joined.drop(columns=['RIVERS_ID','LAYER'])
    joined = joined.reset_index(drop=True)
    joined = joined[['x','y','patch_id','elevation','slope','has_river','has_spring','is_sea','geometry']]

    return joined.to_records(index=False)


def make_sites_df(sites_gdf, cells_gdf):
    site_patch = gpd.sjoin(sites_gdf, cells_gdf[['patch_id','geometry']])
    site_patch = site_patch.drop(columns=['index_right'])
    site_gb_category = site_patch.groupby('patch_id')['category'].agg('first')
    site_gb_timestart = site_patch.groupby('patch_id')['TimeStart'].agg('min')
    site_gb_timeend = site_patch.groupby('patch_id')['TimeEnd'].agg('max')
    index = site_gb_category.index
    sites_on_patch = pd.DataFrame({'category': site_gb_category, 'time_start':site_gb_timestart, 'time_end':site_gb_timeend}, index=index).reset_index()
    
    def site_importance(category):
        if category == 'Villa':
            return 0.1
        elif category == 'House/Houses':
            return 0.2
        elif category == 'Settlement/Village/Site':
            return 0.5
        elif category == 'City':
            return 0.7
        else:
            return 0.9
    
    sites_on_patch['importance'] = sites_on_patch['category'].apply(lambda x: site_importance(x))
    return sites_on_patch


def prepare_environment(raster_path, rivers_shp_path, springs_shp_path, sites_shp_path):

    print("Preparing model environment...")

    import pickle
    try:
        with open('model_cache/patches.pickle', 'rb') as f:
            patches = pickle.load(f)
        sites = pd.read_csv('model_cache/sites.csv')
        grid_dim = (403, 293)
    except:
        print("failed to load cache")
        cells, grid_dim = make_cells_from_raster(raster_path)
        rivers = read_shp(rivers_shp_path)
        springs = read_shp(springs_shp_path)
        sites = read_shp(sites_shp_path)

        patches = make_patches_for_grid(cells, rivers, springs)
        sites = make_sites_df(sites, cells)

        try:
            import os
            #os.mkdir('model_cache')
            with open('model_cache/patches.pickle', 'wb') as f:
                pickle.dump(patches, f)
            sites.to_csv('model_cache/sites.csv', index=False)

        except:
            print("failed to cache model environment")
            pass

    return grid_dim, patches, sites

