# About

This is a Python implementation of an agent-based model of ancient civilization AncientS which was formulated by Angelos Chliaoutakis and Georgios Chalkiadakis (Technical University of Crete) and originally implemented in NetLogo. See https://www.comses.net/codebases/50cf5d2a-0f37-4d4d-84e0-715e641180d9/releases/1.0.0/ for the original model.

Note that we only intend to reimplement the model in Python as a case study for building a spatial ABM from scratch using GIS data. Our other goal was to improve simulation performance of the model. We only ported core functionalities such as simple migration decisions and egalitarian social paradigm. Natural disaster events are not ported. We find inconsistencies between the NetLogo and Python simulation outputs given the default NetLogo parameters values; these inconsistencies will need to be resolved for this Python implementation to be utilized in research production.


# Requirements

- python 3.9+
- jupyter (any)
- geopandas 0.9.0
- matplotlib 3.3.2+
- networkx 2.5+
- numpy 1.20+
- pandas 1.3.4
- rasterio 1.2.10
- richdem 0.3.4
- seaborn 0.11.2


# Setup & Running the Model

- Download "ancients-py-main.zip" from https://gitlab.com/unchitta/ancients-py
- Unzip into an Extract folder of your choosing on your computer
- Open Jupyter Notebook and navigate to the extract folder 
- Open the file "run_model.ipynb"
	o Note: If you only want to test run the model, then change the line of code "model.run(years=2000)" to a smaller value (e.g., 10) so you do not have to run the entire model (which takes us 2 hours on decently powered machines).
- For the first time the model is run, the model environment will be calculated from the GIS data. The model environment will be cached for the subsequent times you run the model to improve initialization speed. To delete the cache, you can delete the contents in the `model_cache` directory.
	