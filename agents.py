import numpy as np
import random
import string
from copy import deepcopy

random.seed(1)


def _id_generator(agenttype, size=6):
    """generates a random unique id for agents
    example: H_864410 for a household
    """
    # prefixes for each type of agent
    # note: in the actual model implementation, we'll only use this with Household agents
    prefixes = {'household': 'H', 'settlement':'S', 'patch':'P'}
    # use string.ascii_uppercase + string.digits for alphanumeric sequence
    uid = ''.join(random.choices(string.digits, k=size))
    return f"{prefixes[agenttype]}_{uid}"



class Patch:
    def __init__(self, uid, 
                elevation, slope, has_river, has_spring, is_sea, polygon,
                land_productivity=None, aquifer_utility=None,
                pos=None):
        self.uid = uid
        self.pos = pos
        self.polygon = polygon
        self.elevation = elevation
        self.slope = slope
        self.has_river = has_river
        self.is_sea = is_sea
        self.is_coastline = False   # set default; will modify this later by the controller in model initialization
        self.has_spring = has_spring
        self.land_productivity = land_productivity
        self.land_suitability = np.exp(-slope/40)
        self.aquifer_utility = aquifer_utility  # not sure about adding this here or a separate setter func
        self.resource = 0
        self.production = 0
        self.production_temp = 0
        self.sr = 1500
        self.is_cultivated = False
        self.has_site = False
        self.site_id = None
        self.site_category = None
        self.site_importance = None

        self.rvalue = int(has_river)
        self.svalue = int(has_spring)
        self.ok_to_migrate = ~is_sea # temp variable to help with migration process
        self.expected_utility = 0 # temp variable to help with migration process

    def establish_site(self, site_id, category, importance):
        self.has_site = True
        self.site_id = site_id
        self.site_category = category
        self.site_importance = importance

    def update_resource(self):
        self.resource = self.land_productivity * self.land_suitability * self.sr
        self.production = self.resource
    
    def update(self):
        self.is_cultivated = False
        self.update_resource()
    
    def agr_production(self, cell_pop, max_ind_cell):
        mu = self.sr*self.land_productivity # initial amount of resource
        mu_max = self.sr
        term1 = ( 2*mu - 4*mu_max ) * ( cell_pop**2 / max_ind_cell**2 )
        term2 = ( 4*mu_max -  3*mu) * ( cell_pop / max_ind_cell )
        production = self.land_suitability * (term1 + term2 + mu)
        if production < 1:
            print(cell_pop, mu, self.land_suitability, term1, term2)
        return production if production >= 0 else 0

    def cultivate(self, cell_pop, max_ind_cell):
        harvest = self.agr_production(cell_pop, max_ind_cell)
        self.is_cultivated = True
        return harvest if harvest >= 0 else 0



class Settlement:
    def __init__(self, uid, importance):
        self.uid = uid if uid is not None else _id_generator('settlement') # this will be the site id
        self.is_active = False
        self.patches = [] # list of patch coords
        self.in_dgr_norm = 0
        self.out_dgr_norm = 0
        self.btw_norm = 0
        self.ws = importance
        self.households = [] # list of household objects
        self.num_households = 0
        self.num_ind = 0
        self.num_cultivating_patches = 0
        self.approx_radius = 0  # approximate radius of catchment (excl. cultivating area)
        self.exchange_received = 0

    def update(self):
        self.num_households = len(self.households)
        self.num_ind = sum(hh.ind for hh in self.households)
        self.num_cultivating_patches = sum(hh.sensory_range for hh in self.households)
        self.approx_radius = (np.sqrt(len(self.patches)) - 1) / 2
        self.exchange_received = 0
        self.check_active()
        

    def check_active(self):
        if self.num_households == 0 or self.num_ind == 0:
            self.is_active = False
        else:
            self.is_active = True

    def add_catchment(self, patch_pos):
        self.patches.append(patch_pos)

    def add_household(self, household_agent):
        if household_agent not in self.households:
            household_agent.sid = self.uid
            self.households.append(household_agent)
            self.num_ind += household_agent.ind
            self.num_households += 1

    def remove_household(self, household_agent):
        try:
            household_agent.old_settlements.append(self.uid)
            self.households.remove(household_agent)
            self.num_ind -= household_agent.ind
            self.num_households -= 1
            household_agent.sid = None
        except:      # household_agent not in this household
            print(household_agent.uid)
            print([hh.uid for hh in self.households])
            print(f"warning: failed to remove household {household_agent.uid} from settlement {self.uid}")   



class Household:
    def __init__(self, sid, individuals, min_resource_per_ind=250, yrs_storage=5):
        self.uid = _id_generator('household')
        self.sid = sid
        self.ind = individuals # number of individuals
        self.pos = None
        self.utility = 0
        self.utility_last_year = 0
        self.sr = 1500 # intensive agricultural technology
        self.min_resource_per_ind = min_resource_per_ind
        self.u_thres = self.calculate_uthres()
        self.sensory_range = self.calculate_sensory_range()
        self.storage = [0 for _ in range(yrs_storage)]
        #self.cultivating_patches = []
        self.num_cpatches = 0
        self.years_to_migrate = 0
        self.migration_patches = []
        self.is_peer = False
        self.is_subordinate = False
        self.is_superior = False
        self.considers_migrating = False
        self.old_settlements = []

    def calculate_uthres(self):
        return self.min_resource_per_ind * self.ind

    def calculate_sensory_range(self):
        return max([np.ceil(self.u_thres / self.sr), 1])

    def add_individual(self, how_many=1):
        self.ind += how_many
        # update min_uthres and sensory range
        self.u_thres = self.calculate_uthres()
        self.sensory_range = self.calculate_sensory_range()

    def remove_individual(self, how_many=1):
        self.ind -= how_many
        # update min_uthres and sensory range
        self.u_thres = self.calculate_uthres()
        self.sensory_range = self.calculate_sensory_range()

    def update(self):
        # store excess utility in storage and reset to 0
        excess_util = self.utility - self.u_thres
        excess_util = 0 if excess_util < 0 else excess_util
        self.utility_last_year = self.utility
        self.utility = 0
        # discard older storage and propagate
        self.storage.pop()
        self.storage.insert(0,excess_util)
        # update u_thres
        self.u_thres = self.calculate_uthres()
        # add 1 to years to migrate
        if (self.utility_last_year < self.u_thres) and (round(sum(self.storage))==0):
            self.years_to_migrate += 1
        elif (self.utility_last_year > self.u_thres):
            self.years_to_migrate = 0
        if self.years_to_migrate >= len(self.storage):
            self.considers_migrating = True
        
        # update number of cultivating patches
        #self.num_cpatches = len(self.cultivating_patches)

    def reset(self):
        self.utility = 0
        self.u_thres = self.ind * self.min_resource_per_ind
        self.sensory_range = max([np.ceil(self.u_thres / self.sr), 1])
        self.storage = [0 for _ in range(len(self.storage))]
        self.cultivating_patches = []
        self.years_to_migrate = 0
        self.migration_patches = []
        self.is_peer = False
        self.is_subordinate = False
        self.is_superior = False
        self.considers_migrating = False

    def split(self):
        # pick a number of individuals to split into a new household
        new_hh_num_ind = random.randrange(1,self.ind)

        # hatch
        new_hh = deepcopy(self) # might have to do a deeop copy on this one
        new_hh.uid = _id_generator('household')
        new_hh.ind = new_hh_num_ind
        new_hh.reset()
        
        self.ind -= new_hh_num_ind

        return new_hh


    def __str__(self):
        return f"Household with {self.ind} individuals at {self.pos}\n\
            current utility: {self.utility}\n\
            current storage: {self.storage}\n\
            years to migrate: {self.years_to_migrate}"
